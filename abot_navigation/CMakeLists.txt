cmake_minimum_required(VERSION 2.8.3)
project(abot_navigation)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
)

catkin_package()

include_directories(${catkin_INCLUDE_DIRS})

catkin_install_python(
PROGRAMS
nodes/teleop
nodes/navigate
nodes/base_controller.py
DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

